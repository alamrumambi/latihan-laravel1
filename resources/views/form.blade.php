<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fn">First name:</label><br><br>
        <input name="first_name" type="text" id="fn"><br><br>
        <label for="ln">Last name:</label><br><br>
        <input name="last_name" type="text" id="ln"><br><br>
        <label for="">Gender:</label><br><br>
        <input type="radio" name="gender" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" id="female" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" id="other" value="other">
        <label for="other">Other</label><br>
        <br>
        <label for="nat">Nationality</label><br><br>
        <select name="nationality" id="nat">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Japanese">Japanese</option>
        </select>
        <br><br>
        <label for="">Language Spoken</label><br><br>
        <input type="checkbox" name="language" id="indo" value="Bahasa Indonesia">
        <label for="indo">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" id="eng" value="English">
        <label for="eng">English</label><br>
        <input type="checkbox" name="language" id="otherc" value="Other">
        <label for="otherc">Other</label><br>
        <br>
        <label for="">Bio</label>
        <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>