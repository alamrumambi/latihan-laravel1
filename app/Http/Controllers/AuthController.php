<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function get_register() {
        return view('form');
    }

    public function post_register(Request $request) {
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $full_name = "$first_name $last_name";
        return view('welcome2', ['full_name' => $full_name]);
    }
}
